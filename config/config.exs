use Mix.Config

config :add_member_bot, Friends.Repo,
  database: "add_member_bot_repo",
  username: "postgres",
  password: "postgres",
  hostname: "localhost"

config :add_member_bot,
  ecto_repos: [Friends.Repo]

config :ex_gram, token: System.get_env("BOT_TOKEN_MEMBERBOT")
