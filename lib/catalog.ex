defmodule AddMemberBot.Catalog do
  import Ecto.Query, warn: false
  import AddMemberBot.CLI.ColorIO
  alias Friends.Repo

  alias Friends.Person

  def insert_user(tg_user_id, attrs \\ %{}) do
    ("tg_user_id : " <> to_string(tg_user_id)) |> green

    %Person{tg_id: tg_user_id}
    |> Person.changeset(attrs)
    |> Repo.insert()
  end

  def list_all_users do
    Repo.all(Person)
  end
end
