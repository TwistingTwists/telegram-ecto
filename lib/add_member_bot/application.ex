defmodule AddMemberBot.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Starts a worker by calling: AddMemberBot.Worker.start_link(arg)
      ExGram,
      {AddMemberBot.Bot, [method: :polling, token: System.get_env("BOT_TOKEN_MEMBERBOT")]},
      {Friends.Repo, []}
      # {AddMemberBot.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: AddMemberBot.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
