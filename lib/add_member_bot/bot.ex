defmodule AddMemberBot.Bot do
  @bot :add_member_bot

  import AddMemberBot.CLI.ColorIO
  alias Friends.Repo
  alias AddMemberBot.Catalog

  use ExGram.Bot,
    name: @bot,
    setup_commands: true

  command("start")
  command("help", description: "Print the bot's help")
  command("addme")

  middleware(ExGram.Middleware.IgnoreUsername)

  def bot(), do: @bot

  def handle({:command, :start, msg}, context) do
    # msg.chat.id |> purple
    # msg.chat.username |> purple
    ExGram.send_message(msg.from.id, "Registering you for the program ... Wait a moment.")
    changeset = Catalog.insert_user(msg.chat.id, %{name: msg.chat.username}) |> log2

    case changeset do
      {:ok, _person} ->
        context |> answer("Successfully Registered.")

      {:error, _} ->
        context
        |> answer(
          "you could not be registered on the bot. Contact @twistingtwists for more details"
        )
    end
  end

  def handle({:command, :help, message} = first, context) do
    first |> red
    group_chat_id = message.chat.id |> green

    case ExGram.create_chat_invite_link(group_chat_id, member_limit: 1) do
      {:ok, chat_invite_link} ->
        chat_invite_link |> log2

        ExGram.send_message(
          message.from.id,
          "got a message of request. wake up " <> chat_invite_link.invite_link
        )

      {:error, error} ->
        "lalala " |> green
    end
  end

  def handle({:command, :addme, _msg}, context) do
    user_id = context.chat.id
    context |> answer("brb")
    # ExGram.create_chat_invite_link(,)
  end

  def handle({:message, message}, context) do
    message |> log2
  end
end
