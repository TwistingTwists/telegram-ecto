defmodule AddMemberBot.CLI.ColorIO do
  @the_types [
    :number,
    :atom,
    :string,
    :boolean,
    :binary,
    :bitstring,
    :list,
    :map,
    :regex,
    :tuple,
    :function,
    :struct,
    :pid,
    :port,
    :reference,
    :date,
    :datetime
  ]
  @green Enum.map(@the_types, fn t -> {t, :green} end)
  @red Enum.map(@the_types, fn t -> {t, :red} end)
  @yellow Enum.map(@the_types, fn t -> {t, :yellow} end)
  @blue Enum.map(@the_types, fn t -> {t, :lightblue} end)
  @purple Enum.map(@the_types, fn t -> {t, IO.ANSI.color(4, 2, 5)} end)
  @orange Enum.map(@the_types, fn t -> {t, IO.ANSI.color(4, 2, 0)} end)

  @multi Enum.map(@the_types, fn t ->
           case t do
             :number -> {t, :yellow}
             :pid -> {t, :red}
             :bitstring -> {t, :cyan}
             :map -> {t, :green}
             :atom -> {t, IO.ANSI.color(4, 2, 0)}
             :date -> {t, :blue}
             :datetime -> {t, :blue}
             :list -> {t, IO.ANSI.color(1, 5, 0)}
             # :string -> {t, IO.ANSI.color(4,2,5)}
             # :binary -> {t, IO.ANSI.color(0,2,5)}
             _ -> {t, IO.ANSI.color(4, 2, 5)}
           end
         end)

  def green(data) do
    IO.inspect(data, syntax_colors: @green)
  end

  def red(data) do
    IO.inspect(data, syntax_colors: @red)
  end

  def yellow(data) do
    IO.inspect(data, syntax_colors: @yellow)
  end

  def blue(data) do
    IO.inspect(data, syntax_colors: @blue)
  end

  def orange(data) do
    IO.inspect(data, syntax_colors: @orange)
  end

  def purple(data) do
    IO.inspect(data, syntax_colors: @purple)
  end

  def log3(data) do
    purple(data)
  end

  def log2(data) do
    orange(data)
  end

  def log(data) do
    IO.inspect(data, syntax_colors: @multi)
  end
end
