defmodule AddMemberBot do
  @moduledoc """
  Documentation for `AddMemberBot`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> AddMemberBot.hello()
      :world

  """
  def hello do
    :world
  end
end
