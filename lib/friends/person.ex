defmodule Friends.Person do
  use Ecto.Schema
  import Ecto.Changeset

  schema "people" do
    field(:name, :string)
    field(:tg_id, :integer)

    # timestamps()
  end

  def changeset(person, params \\ %{}) do
    person
    |> cast(params, [:name, :tg_id])
    |> validate_required([:tg_id])
  end
end
